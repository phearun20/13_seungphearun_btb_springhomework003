package com.example.hw_spring_003.service;


import com.example.hw_spring_003.model.entity.Category;
import com.example.hw_spring_003.model.request.CategoryRequest;
import io.swagger.v3.oas.models.security.SecurityScheme;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();

    Category getCategoryById(Integer categoryId);

    boolean deleteCategoryById(Integer categoryId);

    Integer addNewCategory(CategoryRequest categoryRequest);

    Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId);


}
