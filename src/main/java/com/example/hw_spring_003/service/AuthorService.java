package com.example.hw_spring_003.service;

import com.example.hw_spring_003.model.entity.Author;
import com.example.hw_spring_003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthors();
    Author getAuthorById(Integer authorId);

    boolean deleteAuthorById(Integer authorId);

    Integer addNewAuthor(AuthorRequest authorRequest);
    Integer updateAuthor(AuthorRequest authorRequest, Integer authorId);



}
