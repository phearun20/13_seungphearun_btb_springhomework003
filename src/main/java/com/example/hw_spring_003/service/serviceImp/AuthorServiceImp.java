package com.example.hw_spring_003.service.serviceImp;

import com.example.hw_spring_003.exception.AuthorNotFoundExcepton;
import com.example.hw_spring_003.model.entity.Author;
import com.example.hw_spring_003.model.request.AuthorRequest;
import com.example.hw_spring_003.repository.AuthorRepository;
import com.example.hw_spring_003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthors() {

        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        if (authorId != null){
            return authorRepository.getAuthorById(authorId);
        }else throw new AuthorNotFoundExcepton(authorId);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        return authorRepository.deleteAuthorById(authorId);
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {

        return authorRepository.addNewAuthor(authorRequest);
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        Integer authorIdUpdate = authorRepository.updateAuthor(authorRequest, authorId);
        return authorIdUpdate;
    }
}
