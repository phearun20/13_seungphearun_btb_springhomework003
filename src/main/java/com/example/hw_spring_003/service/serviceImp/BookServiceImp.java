package com.example.hw_spring_003.service.serviceImp;

import com.example.hw_spring_003.model.entity.Book;
import com.example.hw_spring_003.model.entity.Category;
import com.example.hw_spring_003.model.request.BookRequest;
import com.example.hw_spring_003.repository.BookRepository;
import com.example.hw_spring_003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private final BookRepository bookRepository;
    public BookServiceImp(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBook(){
        return bookRepository.getAllBook();
    }

    @Override
    public Book getBookById(Integer bookId){
        return bookRepository.getBookById(bookId);
    }

    @Override
    public boolean deleteBookById(Integer bookId){
        return bookRepository.deleteBookById(bookId);
    }

    @Override
    public Integer addNewBook(BookRequest bookRequest){
        Integer storeBookId = bookRepository.addNewBook(bookRequest);
        for (Integer categoryId : bookRequest.getCategories() ){
            bookRepository.addCategoryByBookId(storeBookId,categoryId);
        }
        return storeBookId;
    }

    @Override
    public Integer updateBook(BookRequest bookRequest, Integer bookId){

        return null;
    }
}
