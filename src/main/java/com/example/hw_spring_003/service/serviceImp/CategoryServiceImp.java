package com.example.hw_spring_003.service.serviceImp;

import com.example.hw_spring_003.model.entity.Category;
import com.example.hw_spring_003.model.request.CategoryRequest;
import com.example.hw_spring_003.repository.CategoryRepository;
import com.example.hw_spring_003.service.CategoryService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAllCategoires();
    }


    @Override
    public Category getCategoryById(Integer categoryId){
        return categoryRepository.getCategoryById(categoryId);
    }

    @Override
    public boolean deleteCategoryById(Integer categoryId) {
        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest){

        return categoryRepository.addNewCategory(categoryRequest);
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId){

        return categoryRepository.updateCategory(categoryRequest, categoryId);
    }
}
