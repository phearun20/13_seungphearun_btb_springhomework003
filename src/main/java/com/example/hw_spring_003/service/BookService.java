package com.example.hw_spring_003.service;

import com.example.hw_spring_003.model.entity.Book;
import com.example.hw_spring_003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();

    Book getBookById(Integer bookId);

    boolean deleteBookById(Integer bookId);

    Integer addNewBook(BookRequest bookRequest);

    Integer updateBook(BookRequest bookRequest, Integer bookId);


}
