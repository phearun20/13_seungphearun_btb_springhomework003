package com.example.hw_spring_003.exception;

public class CategoryNotFoundException extends RuntimeException{
    public CategoryNotFoundException(Integer categoryId){
        super(categoryId+"category Not Found");
    }
}
