package com.example.hw_spring_003.exception;

public class BookNotFoundException extends RuntimeException{
    public BookNotFoundException(Integer bookId){
        super(bookId+"category Not Found");
    }
}
