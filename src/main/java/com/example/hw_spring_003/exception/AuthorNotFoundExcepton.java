package com.example.hw_spring_003.exception;

public class AuthorNotFoundExcepton extends RuntimeException{
    public AuthorNotFoundExcepton(Integer authorId){
        super("User with id :" + authorId + " not Found");
    }
}
