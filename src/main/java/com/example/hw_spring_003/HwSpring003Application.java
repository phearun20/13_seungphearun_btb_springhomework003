package com.example.hw_spring_003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HwSpring003Application {

    public static void main(String[] args) {
        SpringApplication.run(HwSpring003Application.class, args);
    }

}
