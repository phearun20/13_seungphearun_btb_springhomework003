package com.example.hw_spring_003.controller;

import com.example.hw_spring_003.exception.CategoryNotFoundException;
import com.example.hw_spring_003.model.entity.Category;
import com.example.hw_spring_003.model.request.CategoryRequest;
import com.example.hw_spring_003.model.response.AuthorResponse;
import com.example.hw_spring_003.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get All Categories")
    public ResponseEntity<AuthorResponse<List<Category>>> getAllCategories(){
        AuthorResponse<List<Category>> response = AuthorResponse.<List<Category>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(HttpStatus.OK)
                .message("Successfully fetched Categories")
                .payload(categoryService.getAllCategories())
                .build();
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{categoryId}")
    @Operation(summary = "Get Category By Id")
    public ResponseEntity<AuthorResponse<Category>> getCaegoryById(Integer categoryId){
        if (categoryService.getCategoryById(categoryId)!=null){
            AuthorResponse<Category> response = AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Succesfully fetch category")
                    .payload(categoryService.getCategoryById(categoryId))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new CategoryNotFoundException(categoryId);
        }


    }


    @DeleteMapping("/{categoryId}")
    @Operation(summary = "Delete Category By Id")
    public ResponseEntity<AuthorResponse<Category>> deleteCategoryById(Integer categoryId){
        AuthorResponse<Category> response = null;
        if (categoryService.deleteCategoryById(categoryId) == true){
             response = AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Deleted Category Successfully")
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new CategoryNotFoundException(categoryId);
        }


    }


    @PostMapping
    @Operation(summary = "Add NEW Category")
    public ResponseEntity<AuthorResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        Integer storeCategoryId = categoryService.addNewCategory(categoryRequest);
        System.out.println(storeCategoryId);
        if (storeCategoryId != null){
            AuthorResponse<Category> response = AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Add Category Successfully")
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .build();
            return ResponseEntity.ok(response);
        }
return null;
    }


    @PutMapping("/{categoryId}")
    @Operation(summary = "Update Category By Id")
    public ResponseEntity<AuthorResponse<Category>> updateCategory(@RequestBody CategoryRequest categoryRequest, Integer categoryId){
        AuthorResponse<Category> response = null;
        Integer idCategoryUpdate = categoryService.updateCategory(categoryRequest, categoryId);
        if (idCategoryUpdate != null){
            response = AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Updated Category Successfully")
                    .payload(categoryService.getCategoryById(idCategoryUpdate))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}
