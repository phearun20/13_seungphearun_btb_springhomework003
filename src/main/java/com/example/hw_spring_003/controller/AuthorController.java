package com.example.hw_spring_003.controller;

import com.example.hw_spring_003.exception.AuthorNotFoundExcepton;
import com.example.hw_spring_003.model.entity.Author;
import com.example.hw_spring_003.model.request.AuthorRequest;
import com.example.hw_spring_003.model.response.AuthorResponse;
import com.example.hw_spring_003.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;
    public AuthorController(AuthorService authorService) {

        this.authorService = authorService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all authors")
    public ResponseEntity<AuthorResponse<List<Author>>> getAllAuthors() {
        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(HttpStatus.OK)
                .message("Successfully fetch authors")
                .payload(authorService.getAllAuthors())
                .build();
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{authorId}")
    @Operation(summary = "Get author by Id")

    public ResponseEntity<AuthorResponse<Author>> getAuthorById(Integer authorId) {
        if (authorService.getAuthorById(authorId)!=null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully fetch author by Id")
                    .payload(authorService.getAuthorById(authorId))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new AuthorNotFoundExcepton(authorId);
        }

    }


    @DeleteMapping("/{id}")
    @Operation(summary = "Delete author by id")
    public ResponseEntity<AuthorResponse<Author>> deleteAuthorById(@PathVariable("id") Integer authorId) {
        AuthorResponse<Author> response = null;
        if (authorService.deleteAuthorById(authorId) == true){
              response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully Deleted author")
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new AuthorNotFoundExcepton(authorId);
        }

    }


    @PostMapping
    @Operation(summary = "Add new Author")
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){
        Integer storeAuthorId = authorService.addNewAuthor(authorRequest);
        System.out.println(storeAuthorId);
        if (storeAuthorId != null){
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully add Author")
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update author by Id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById
            (@RequestBody AuthorRequest authorRequest, @PathVariable("id") Integer authorId){
        AuthorResponse<Author> response = null;
        Integer idAuthorUpdate = authorService.updateAuthor(authorRequest, authorId);
        if (idAuthorUpdate != null){
            response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully updated")
                    .payload(authorService.getAuthorById(idAuthorUpdate))
                    .build();
        }
        return ResponseEntity.ok(response);
    }







}
