package com.example.hw_spring_003.controller;

import com.example.hw_spring_003.exception.BookNotFoundException;
import com.example.hw_spring_003.model.entity.Book;
import com.example.hw_spring_003.model.request.BookRequest;
import com.example.hw_spring_003.model.response.AuthorResponse;
import com.example.hw_spring_003.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/books")
public class BookController {
    private final BookService bookService;
    public BookController(BookService bookService){
        this.bookService = bookService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get All Books")
    public ResponseEntity<AuthorResponse<List<Book>>> getAllBooks() {
        AuthorResponse<List<Book>> response = AuthorResponse.<List<Book>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .httpStatus(HttpStatus.OK)
                .message("Successfully fetched boos")
                .payload(bookService.getAllBook())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{bookId}")
    @Operation(summary = "Get Book by Id")
    public  ResponseEntity<AuthorResponse<Book>> getBookById(Integer bookId){
        if (bookService.getBookById(bookId) != null){
            AuthorResponse<Book> response = AuthorResponse.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully fetch Book")
                    .payload(bookService.getBookById(bookId))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            throw new BookNotFoundException(bookId);
        }

    }


    @DeleteMapping("/{id}")
    @Operation(summary = "Delete book by Id")
    public ResponseEntity<AuthorResponse<String>> deleteBookById(@PathVariable("id") Integer bookId){
        AuthorResponse<String> response = null;
        if (bookService.deleteBookById(bookId) == true){
            response = AuthorResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Successfully deleted book")
                    .build();
            return ResponseEntity.ok(response);
        }else {
            throw new BookNotFoundException(bookId);
        }

    }


    @PostMapping("/add")
    @Operation(summary = "Add new Book")
    public ResponseEntity<AuthorResponse<Book>> addNewBook(@RequestBody BookRequest bookRequest){
        AuthorResponse<Book> response = null;
        Integer storeBookId = bookService.addNewBook(bookRequest);
        if (storeBookId != null){
            response = AuthorResponse.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .httpStatus(HttpStatus.OK)
                    .message("Add succesfully")
                    .payload(bookService.getBookById(storeBookId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }
}
