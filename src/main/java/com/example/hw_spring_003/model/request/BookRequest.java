package com.example.hw_spring_003.model.request;

import com.example.hw_spring_003.model.entity.Author;
import com.example.hw_spring_003.model.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookRequest {
    private String title;
    private Timestamp published_date;
    private Integer author;
    private List<Integer> categories = new ArrayList<>();
}
