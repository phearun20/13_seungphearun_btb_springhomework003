package com.example.hw_spring_003.repository;

import com.example.hw_spring_003.model.entity.Book;
import com.example.hw_spring_003.model.request.BookRequest;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("SELECT * FROM books")
    @Results(
            id = "bookMapper",
            value = {
                    @Result(property = "book_id", column = "book_id"),
                    @Result(property = "published_date", column = "published_date"),
                    @Result(property = "title", column = "title"),
                    @Result(property = "author", column = "author_id",
                            one = @One(select = "com.example.hw_spring_003.repository.AuthorRepository.getAuthorById")),
                    @Result(property = "categories", column = "book_id",
                            many = @Many(select = "com.example.hw_spring_003.repository.CategoryRepository.getCategoryByBookId"))
            }
    )
    List<Book> getAllBook();

    @Select("SELECT * FROM books WHERE book_id = #{bookId} ")
    @ResultMap("bookMapper")
    Book getBookById(Integer bookId);

    @Delete("DELETE FROM books WHERE book_id = #{bookId} ")
    boolean deleteBookById(Integer bookId);

    @Select("INSERT INTO books (title, published_date, author_id) " +
            "VALUES (#{request.title}, #{request.published_date}, #{request.author}) " +
            "RETURNING book_id")
    Integer addNewBook(@Param("request") BookRequest bookRequest);
    @Select("INSERT INTO book_details (book_id, category_id) " +
            "VALUES (#{bookId},#{categoryId})")
    Integer addCategoryByBookId(Integer bookId, Integer categoryId);

     @Delete("DELETE FROM books WHERE book_id = #{bookId} ")
    Integer deleteBook(BookRequest bookRequest, Integer bookId);
    @Select("UPDATE books " +
            "SET book_title = #{request.book_title}, " +
            "published_date = #{request.published_date} " +
            "author_id = #{request.author} " +
            "WHERE book_id = #{bookId} " +
            "RETURNING book_id")
    Integer updateBook(@Param("request")BookRequest bookRequest,Integer bookId);
    @Select("INSERT INTO books (title, published_date, author_id) " +
            "VALUES (#{request.title}, #{request.published_date}, #{request.author}) " +
            "RETURNING book_id")
    Integer addBook(@Param("request") BookRequest bookRequest);
    @Select("INSERT INTO book_details (book_id, category_id) " +
            "VALUES (#{bookId},#{categoryId})")
    Integer addBookId(Integer bookId, Integer categoryId);



}
