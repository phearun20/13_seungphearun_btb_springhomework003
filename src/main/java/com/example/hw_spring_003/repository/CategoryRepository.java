package com.example.hw_spring_003.repository;

import com.example.hw_spring_003.model.entity.Category;
import com.example.hw_spring_003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("SELECT c.category_id, c.category_name FROM book_details bd " +
            "INNER JOIN categories c ON c.category_id = bd.category_id " +
            "WHERE bd.book_id = #{bookId}")
    List<Category> getCategoryByBookId(Integer bookId);

//    @Select("SELECT p.productid, p.productname, p.productprice FROM invoicedetail ind " +
//            "INNER JOIN products p ON p.productid = ind.productid " +
//            "WHERE ind.invoiceid = #{invoiceId}")
//    List<Product> getProductByInvocieId(Integer invoiceId);
    @Select("SELECT * FROM categories")
    List<Category> findAllCategoires();

    @Select("SELECT * FROM categories WHERE category_id = #{categoryId}")
    Category getCategoryById(Integer categoryId);

    @Delete("DELETE FROM categories WHERE category_id = #{categoryId}")
    boolean deleteCategoryById(Integer categoryId);
    @Select("INSERT INTO categories (category_name) VALUES(#{request.category_name})" +
            "RETURNING category_id")
    Integer addNewCategory(@Param("request") CategoryRequest categoryRequest);



    @Select("UPDATE categories " +
            "SET category_name = #{request.category_name} " +
            "WHERE category_id = #{categoryId} " +
            "RETURNING category_id")
    Integer updateCategory(@Param("request")CategoryRequest categoryRequest, Integer categoryId);
}
